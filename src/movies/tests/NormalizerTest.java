package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;


   class NormalizerTest {
	   
		private Normalizer x=new Normalizer("src\\texts\\results","src\\texts\\normalized");

	   
   @Test
	void testNormalizeProcess() {
	   
	   
		ArrayList<String> testNormalizer = new ArrayList<String>();
		
		testNormalizer.add("2008	the Mummy: toMb of the draGon emperor	112 hello	kaggle");
		testNormalizer.add("2008	the Mummy: toMb of the draGon emperor	112 hello	kaggle");

		
		String expected ="2008	the mummy: tomb of the dragon emperor	112	kaggle";
		assertEquals(expected,x.process(testNormalizer).get(0));

		
	}

}
