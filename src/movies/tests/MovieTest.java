package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class MovieTest {

	@Test
	void testMovie() {
	Movie m = new Movie("1999","Mr Mouse","90","imdb");
	if(m.toString().equals("1999	Mr Mouse	90	imdb")) {
	assertEquals(0,0);
	}
	}

	@Test
	void testEqualsObject() {
		Movie m1 = new Movie("1999","Mr Mouse","90","imdb");
		Movie m2 = new Movie("1999","Mr Mouse","94","imdb");
		Movie m3 = new Movie("1999","Mr Mouse","90","imdb");
		Movie m4 = new Movie("1998","Mr Mouse","90","imdb");

 		ArrayList<Movie> movies = new ArrayList<Movie>();
 		
		int check = 0;
		
		//Check if any movie
		movies.add(m1);
		if(movies.contains(m1) == true) {
			check++;
		}
		//Check if minutes
		if(movies.contains(m2) == true) {
			check++;
		}
		//Check if movie not added
		if(movies.contains(m3) == true) {
			check++;
		}
		//Check if year or title different = different film
		movies.add(m2);
		movies.add(m3);
		if(movies.contains(m4) == true) {
			check++;
		}
		
		assertEquals(3,check); // supposed to be 3
		
	}

	@Test
	void testGetReleaseYear() {
		Movie m = new Movie("1999","Mr Mouse","90","imdb");
		if(m.getReleaseYear().equals("1999")) {
			assertEquals(0,0);
			}
	}

	@Test
	void testGetMovieName() {
		Movie m = new Movie("1999","Mr Mouse","90","imdb");
		if(m.getMovieName().equals("Mr Mouse")) {
			assertEquals(0,0);
			}
	}

	@Test
	void testGetRuntime() {
		Movie m = new Movie("1999","Mr Mouse","90","imdb");
		if(m.getRuntime().equals("90")) {
			assertEquals(0,0);
			}
	}

	@Test
	void testGetSource() {
		Movie m = new Movie("1999","Mr Mouse","90","imdb");
		if(m.getSource().equals("imdb")) {
			assertEquals(0,0);
			}
	}

	@Test
	void testToString() {
		Movie m = new Movie("1999","Mr Mouse","90","imdb");
		if(m.toString().equals("1999	Mr Mouse	90	imdb")) {
			assertEquals(0,0);
			
		}
		
		
	}

}
