package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;



class KaggleImporterTest {
	
	private KaggleImporter x =new KaggleImporter("src\\texts\\kaggle","src\\texts\\results");

	@Test
	void testKaggleProcess() {
		
		
		ArrayList<String> testKaggle = new ArrayList<String>();

		testKaggle.add("one	two	three	four	five	six	seven	eight	nine	ten	eleven	twelve	10/13/2008	112	fiveteen	the mummy: tomb of the dragon emperor	seventeen	eighteen	nineteen	twenty	twentyone");
		testKaggle.add("one	two	three	four	five	six	seven	eight	nine	ten	eleven	twelve	10/13/2008	112	fiveteen	the mummy: tomb of the dragon emperor	seventeen	eighteen	nineteen	twenty	twentyone");

		testKaggle.add("one	two	three	four	five	six	seven	eight	nine	ten	eleven	twelve	10/13/2008	112	fiveteen	the mummy: tomb of the dragon emperor	seventeen	eighteen	nineteen	twenty	twentyone");

		String expected ="10/13/2008	the mummy: tomb of the dragon emperor	112	kaggle";
		assertEquals(expected,x.process(testKaggle).get(0));
		
	}

}
  