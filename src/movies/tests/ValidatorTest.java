package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;

class ValidatorTest {

	@Test
	void testProcess() {
	ArrayList<String> validate = new ArrayList<String>();
	
	validate.add("1894	miss jerry	45	imdb");
	validate.add("1894	mister jerry	45	imdb");
	validate.add("1894	miss jerry	45minutes	kaggle");
	validate.add("05/09/1894	miss jerry	45	kaggle");
	validate.add("1894	miss jerry	kaggle");
	int correct = 0; // 2 is correct
	int lineSize = validate.get(0).split("\\t").length;
	String imdbMovie = "";
	String[] splitted = new String[lineSize*validate.size()];

	for(int i = 0;i<validate.size();i++) {
	splitted = validate.get(i).split("\\t");
	try {
	Integer.parseInt(splitted[0]);
	Integer.parseInt(splitted[2]);
	if(splitted[0] != "" && splitted[0] != null && splitted[1] != "" && splitted[1] != null && splitted[2] != "" && splitted[2] != null) {	
		Movie m = new Movie(splitted[0],splitted[1],splitted[2],splitted[3]);
		imdbMovie = m.toString();
		
		if(imdbMovie.equals(validate.get(i))) {
			correct++;
		}
		
		
		}
		}
	
	catch(NumberFormatException exception) {
		System.out.println("Invalid Movie");
	}
	}
	
	assertEquals(correct,2); // 2 are correct
	
	
	}
}

	
