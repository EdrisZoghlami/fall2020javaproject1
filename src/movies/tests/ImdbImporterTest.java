package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class ImdbImporterTest {

	@Test
	void testProcess() {
		
		ArrayList<String> input = new ArrayList<String>();
		input.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		input.add("tt0000009	Miss Jerry	1894	1894-10-09	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		input.add("tt0000009	1894	Miss Jerry	Miss Jerry	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		
		ArrayList<String> check = new ArrayList<String>();
		int lineSize = input.get(0).split("\\t").length;
		String[] splitted = new String[lineSize*input.size()];
		String imdbMovie = "";
		
		for(int i = 0;i<input.size();i++) {
			splitted = input.get(i).split("\\t");
			if(splitted.length == lineSize) {
			Movie m = new Movie(splitted[3],splitted[1],splitted[6],"imdb");
			imdbMovie = m.toString();
			check.add(imdbMovie);
			
			}
			
		}
		assertEquals(2,check.size()); //Should be 2
		
		
		
		
	}

	

}
