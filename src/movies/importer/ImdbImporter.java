package movies.importer;

import java.util.ArrayList;

public class ImdbImporter extends Processor {

/**
 * Creates a ImdbImporter object extending from the processor class.
 * @author elidjay
 * 
 * @param sourceDir
 * Source of text file
 * @param outputDir
 * Output of imported data
 * @param srcContainsHeader
 * Skip first line or not
 */
	
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

/**
 * Takes an array created from source input file and outputs correct data in another file.
 * 
 * @param ArrayList<String> input
 * Array created from the input text file
 * 
 * @return imdb
 * Returns new ArrayList<String> with Movie type data.
 * 
 */
	
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		//Variable declaration
		int lineSize = input.get(0).split("\\t").length;
		String[] splitted = new String[lineSize*input.size()];
		String imdbMovie = "";
		ArrayList<String> imdb = new ArrayList<String>();
		//Runs through the whole array
		for(int i = 0;i<input.size();i++) {
			//Splits every line of txt file
			splitted = input.get(i).split("\\t");
			//Has to have correct amount of columns			
			splitted = input.get(i).split("\\t");
			if(splitted.length == lineSize) {
			Movie m = new Movie(splitted[3],splitted[1],splitted[6],"imdb");
			//Adds to array
			imdbMovie = m.toString();
			imdb.add(imdbMovie);
			
			}
			
		}
		
		return imdb;
	}

}