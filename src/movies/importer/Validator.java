package movies.importer;

import java.util.ArrayList;

public class Validator extends Processor {


/**
 * Creates a validator object extending from the processor class.
 * 
 * @author elidjay
 * 
 * @param sourceDir 
 * Takes the source of a text file
 * @param outputDir
 * Takes where you want the outputed text file
 */
public Validator(String sourceDir, String outputDir) {
        super(sourceDir, outputDir, false);
}

/**
 * Processes a text file of validator type.
 * 
 * @author elidjay
 * 
 * @param input
 * Takes a ArrayList<String> that was read from the text file
 * 
 * @return validated
 * Returns a validated array;
 */

	public ArrayList<String> process(ArrayList<String> input) {
		//Variables declaration
		ArrayList<String> validated = new ArrayList<String>();
		int lineSize = input.get(0).split("\\t").length;
		String imdbMovie = "";
		String[] splitted = new String[lineSize*input.size()]; 
		
		//Loops through every line of the array
		for(int i = 0;i<input.size();i++) {
		splitted = input.get(i).split("\\t");
		//If the releaseYear and runtime are ints continues with program
		try {
		Integer.parseInt(splitted[0]);
		Integer.parseInt(splitted[2]);
		//Checks if no null or empty
		if(splitted[0] != "" && splitted[0] != null && splitted[1] != "" && splitted[1] != null && splitted[2] != "" && splitted[2] != null) {	
			Movie m = new Movie(splitted[0],splitted[1],splitted[2],splitted[3]);
		//Adds to validated array
			imdbMovie = m.toString();
			validated.add(imdbMovie);
			}
			}
		
		catch(NumberFormatException exception) {
		//	System.out.println(input.get(i));
		}
		}
		
		return validated;
	}

}