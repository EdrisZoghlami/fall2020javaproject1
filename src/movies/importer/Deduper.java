package movies.importer;

import java.util.ArrayList;

/**
 * The Deduper Class extends the Processor Class
 * @author Elidjay Ross Edris Zoghlami
 *
 */

public class Deduper extends Processor {

	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
		
	}
	
	

	/**
	 * Takes as input a ArrayList<String> and removes all duplicates movies from that ArrayList.
	 * A movie is a duplicate if they have the same release year, the same title and the runtime between the two is no more then 5 minutes.
	 * if the movie appears in both the kaggle feed and tehh imdb feed the source should be changed to either kaggle;imdb or imdb;kaggle
	 * @return ArrayList<String> representing a List of movies with no duplicates.
	 */
	public ArrayList<String> process(ArrayList<String> input) {
        ArrayList<String> marray = new ArrayList<String>();
         ArrayList<Movie> movies = new ArrayList<Movie>();
        String[] splitted = new String[4];
        int index = 0;
        String source1 = "";
        String source2 = "";
        
        
        for (int i=0;i<input.size();i++) {
            splitted = input.get(i).split("\\t");
            Movie m = new Movie(splitted[0],splitted[1],splitted[2],splitted[3]);
            if(movies.contains(m) == false) {
            movies.add(m);
            marray.add(input.get(i));
            }
            
            else {
                index =    movies.indexOf(m);
                source1 = movies.get(index).getSource();
                source2 = splitted[3];

                if(source1.equals(source2)) {
                }
                else {
                    marray.set(index,splitted[0] + "  " + splitted[1] + "  " + splitted[2] + "  " + "imdb;kaggle");
                }
                }
            
        }
        return marray;
	}
}


	
		

			
					


				