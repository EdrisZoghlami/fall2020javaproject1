package movies.importer;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @author Edris Zoghlami Elidjay Ross
 *
 */
public class ImportPipeline {

	/**
	 * This method creates and instantiates a Processor[] files with processor needed to load feed of both files
	 * normalize them, validate them and dedupe them.
	 */
	public static void main(String[] args) throws IOException {
		
		String input = "src\\texts\\normalized";
		String output = "src\\texts\\results";
	Processor[] p = new Processor[5];
	
	p[0]= new ImdbImporter("src\\texts\\imdb",output);
	p[1]= new KaggleImporter("src\\texts\\kaggle",output);
	p[2]= new Normalizer(output,input);
	p[3]= new Validator(input,"src\\texts\\validated");
	p[4]= new Deduper("src\\texts\\validated", "src\\texts");

    processAll(p);

	
	}
	
	/**
	 * This method takes as input a Processor[] and calls the execute method on each of the method inside of it
	 * @param p representing a Processor[]
	 * @throws IOException
	 */
	public static void processAll(Processor[] p) throws IOException{

		for(int i=0;i<p.length;i++) {
	
		p[i].execute();
		
		
		}
	}

}
