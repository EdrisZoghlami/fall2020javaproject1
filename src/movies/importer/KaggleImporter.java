package movies.importer;

import java.util.ArrayList;



/**
 * A kaggleImporter class that extends the Processor class
 * @author Edris Zoghlami
 *
 */

public class KaggleImporter extends Processor {

	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	
	/**
	 * Takes as input and reads a ArrayList<String> and produces a new ArrayList<String> 
	 * that are standardized to match what a Movie should look like.
	 * @param ArrayList<String>
	 * @return ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> kaggle = new ArrayList<String>();
		
		int lineSize = input.get(0).split("\\t").length;
		
		String [] splitted=new String[lineSize];
		
		String s="";

		
		
		for(int i=0;i<input.size();i++) {
			
			
			splitted = input.get(i).split("\\t");
			
			
			
			if(splitted.length == lineSize) {
			Movie kaggleMovie = new Movie(splitted[12],splitted[15],splitted[13],"kaggle");
			s=kaggleMovie.toString();
			kaggle.add(s);
			}
	
		}
		return kaggle;
	
		
	}

}
