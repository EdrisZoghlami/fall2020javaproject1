package movies.importer;

import java.util.ArrayList;



/**
 * A Normalizer class that extends the Processor class.
 * @author Edris Zoghlami
 */

public class Normalizer extends Processor {

	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
		
	}

	/**
	 * 
	 * This method takes as input a ArrayList<String> and produces a new ArrayList<STring>
	 * in which each of the new Strings represent Movies objects that have been normalized.
	 * To normalized a movie. All the titles must be in lower case and the runtime must only be one word long
	 * @param ArrayList<String>
	 * @return ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {
				
		ArrayList<String> normalized = new ArrayList<String>();
		String s;
		
		
		for(int i=0;i<input.size();i++) {
			
			
					   
			s = input.get(i);
			String [] splitted = s.split("\\t");
			
			if(splitted[0].split("/").length==3) {
				splitted[0]=splitted[0].split("/")[2];
			}
			
			splitted[1]=splitted[1].toLowerCase();
				
			splitted[2]=splitted[2].split("\s")[0];
				
			
			
			Movie normalizedMovie = new Movie(splitted[0],splitted[1],splitted[2],splitted[3]);
			normalized.add(normalizedMovie.toString());
			
			
			}


		return normalized;
	}

}

