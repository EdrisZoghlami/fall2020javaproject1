package movies.importer;

/**
 * 
 * This class represents a movie.
 * @author Edris Zoghlami Elidjay Ross
 * 
 */



public class Movie {
	
	/**
	 * The release year of the movie. 
	 */
	private String releaseYear;
	/**
	 * The name of the movie.
	 */
	private String movieName;
	/**
	 * The run time of the movie
	 */
	private String runtime;
	/**
	 * The source is where the movie came from
	 */
	private String source;
	
	/**
	 * Contructor that creates a new movie with the given release year, movie name, runtime and source of the movie
	 * 
	 * @param releaseYear
	 * @param movieName
	 * @param runtime
	 * @param source
	 */
	

	public Movie(String releaseYear, String movieName, String runtime, String source) {
		this.releaseYear= releaseYear;
		this.movieName=movieName;
		this.runtime = runtime;
		this.source=source;
		
	}
	
	/**
	 * Overrides the equals method
	 * 
	 * @param o 
	 * Movie object as input
	 * 
	 * @return true
	 * If it equals returns true
	 * @return false
	 * If its not equal return false
	 * 
	 */
	
	
	@Override
    public boolean equals(Object o) {
        String[] splitted = o.toString().split("\\t");
        int runtimeDifference = Math.abs(Integer.parseInt(getRuntime()) - Integer.parseInt(splitted[2]));

        if(getReleaseYear().equals(splitted[0]) && getMovieName().equals(splitted[1]) && runtimeDifference <= 5){
        return true;
        }

        return false;
    }
	
	
	/**
	 * Gets the release year of the movie.
	 * @return this movie's release year.
	 */
	public String getReleaseYear() {
		return this.releaseYear;
	}
	
	/**
	 * Gets the name of the movie. 
	 * @return this movie's title.
	 */
	public String getMovieName() {
		return this.movieName;
	}
	
	/**
	 * Gets the runtime of the movie.
	 * @return this movie's runtime.
	 */
	public String getRuntime() {
		return this.runtime;
	}
	
	/**
	 * Gets the source of the movie.
	 * @return this movie's source
	 */
	public String getSource() {
		return this.source;
	}
	
	/**
	 * Turns the movie object into a readable String.
	 */
	public String toString() {
		
		return releaseYear+"	"+movieName+"	"+runtime+"	"+source;
	}
	
	

}

